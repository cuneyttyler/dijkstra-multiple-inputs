package com.semanticspace.neo4j;

import org.junit.jupiter.api.Test;
import org.neo4j.gds.TestSupport;
import org.neo4j.gds.beta.pregel.Pregel;
import org.neo4j.gds.core.concurrency.Pools;
import org.neo4j.gds.core.utils.progress.tasks.ProgressTracker;
import org.neo4j.gds.extension.GdlExtension;
import org.neo4j.gds.extension.GdlGraph;
import org.neo4j.gds.extension.Inject;
import org.neo4j.gds.extension.TestGraph;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@GdlExtension
public class DijktstraMultipleInputsAlgoTest {
    @GdlGraph
    private static final String MY_TEST_GRAPH =
            "CREATE" +
                    "  (n0:Label)" +
                    ", (n1:Label)" +
                    ", (n2:Label)" +
                    ", (n3:Label)" +
                    ", (n4:Label)" +
                    ", (n5:Label)" +
                    ", (n6:Label)" +

                    ", (n0)-[:TYPE {cost: 6}]->(n1)" +
                    ", (n0)-[:TYPE {cost: 2}]->(n2)" +
                    ", (n0)-[:TYPE {cost: 16}]->(n3)" +
                    ", (n1)-[:TYPE {cost: 4}]->(n4)" +
                    ", (n1)-[:TYPE {cost: 5}]->(n3)" +
                    ", (n2)-[:TYPE {cost: 7}]->(n1)" +
                    ", (n2)-[:TYPE {cost: 3}]->(n4)" +
                    ", (n2)-[:TYPE {cost: 8}]->(n5)" +
                    ", (n3)-[:TYPE {cost: 7}]->(n2)" +
                    ", (n4)-[:TYPE {cost: 4}]->(n3)" +
                    ", (n4)-[:TYPE {cost: 10}]->(n6)" +
                    ", (n5)-[:TYPE {cost: 1}]->(n6)";


    @Inject
    private TestGraph graph;

    @Test
    void runExamplePregelComputation() {
        int maxIterations = 10;

        var config = ImmutableMyPregelConfig.builder()
                .maxIterations(maxIterations)
                .startNode(0)
                .endNode(6)
                .build();

        var pregelJob = Pregel.create(
                graph,
                config,
                new DijkstraMultipleInputs(),
                Pools.DEFAULT,
                ProgressTracker.NULL_TRACKER
        );

        var result = pregelJob.run();

        assertTrue(result.didConverge(), "Algorithm did not converge.");
        assertEquals(0, result.ranIterations());

        var expected = new HashMap<String, Long>();
        expected.put("n0", 0L);
        expected.put("bob", 1L);
        expected.put("eve", 2L);

        TestSupport.assertLongValues(graph, (nodeId) -> result.nodeValues().longValue(DijkstraMultipleInputs.DISTANCE, nodeId), expected);
    }
}
