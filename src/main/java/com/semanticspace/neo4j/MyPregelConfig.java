package com.semanticspace.neo4j;

import org.neo4j.gds.annotation.Configuration;
import org.neo4j.gds.annotation.ValueClass;
import org.neo4j.gds.beta.pregel.PregelProcedureConfig;
import org.neo4j.gds.core.CypherMapWrapper;


@ValueClass
@Configuration
@SuppressWarnings("immutables:subtype")
public interface MyPregelConfig extends PregelProcedureConfig {
    long startNode();
    long endNode();
    static MyPregelConfig of(CypherMapWrapper userInput) {
        return new MyPregelConfigImpl(userInput);
    }

}