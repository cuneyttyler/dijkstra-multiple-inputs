package com.semanticspace.neo4j;

import org.neo4j.gds.api.nodeproperties.ValueType;
import org.neo4j.gds.beta.pregel.Messages;
import org.neo4j.gds.beta.pregel.PregelComputation;
import org.neo4j.gds.beta.pregel.PregelSchema;
import org.neo4j.gds.beta.pregel.Reducer;
import org.neo4j.gds.beta.pregel.annotation.GDSMode;
import org.neo4j.gds.beta.pregel.annotation.PregelProcedure;
import org.neo4j.gds.beta.pregel.context.ComputeContext;
import org.neo4j.gds.beta.pregel.context.InitContext;
import org.neo4j.gds.beta.pregel.context.MasterComputeContext;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Setting the value for each node to the node-id of its parent.
 * If there are multiple parents at the discovery level/iteration, the parent with the minimum id is chosen.
 */
@PregelProcedure(name = "semanticspace.gds.pregel.dijkstraMultiplePairs", modes = {GDSMode.STREAM})
public class DijkstraMultipleInputs implements PregelComputation<MyPregelConfig> {

    public static final long NOT_FOUND = Long.MAX_VALUE;
    public static final String DISTANCE = "distance";
    public static final String PATH = "path";

    @Override
    public PregelSchema schema(MyPregelConfig config) {
        return new PregelSchema.Builder()
                .add(DISTANCE, ValueType.LONG, PregelSchema.Visibility.PUBLIC)
                .build();
    }

    public void init(InitContext<MyPregelConfig> context) {
    }

    @Override
    public void compute(ComputeContext<MyPregelConfig> context, Messages messages) {
        long nodeId = context.nodeId();

        if (context.isInitialSuperstep()) {
            if (nodeId == context.config().startNode()) {
                context.logMessage("Processing start node, degree = " + context.degree());
                context.sendToNeighbors(1);
                context.voteToHalt();
            } else {
                context.setNodeValue(DISTANCE, NOT_FOUND);
            }
        } else {
            long distance = context.longNodeValue(DISTANCE);
            if(messages.iterator().hasNext()) {
                distance = Long.min(distance,messages.iterator().next().longValue());
//                context.logMessage("RECEIVED MESSAGE : " + distance + ", NODE : " + nodeId);
            }

            if(distance != NOT_FOUND) {
                context.setNodeValue(DISTANCE,distance);
                context.sendToNeighbors(distance+1);
                context.voteToHalt();
            }
        }
    }

    @Override
    public boolean masterCompute(MasterComputeContext<MyPregelConfig> context) {
        long endNodeDistance = context.longNodeValue(context.config().endNode(),DISTANCE);
        if(endNodeDistance != NOT_FOUND) {
            context.logMessage("FOUND! Distance = " + endNodeDistance);
            return true;
        } else {
            context.logMessage("NOT FOUND!");
            return false;
        }
    }

    @Override
    public Optional<Reducer> reducer() {
        return Optional.of(new Reducer.Min());
    }
}
