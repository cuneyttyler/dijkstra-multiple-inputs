package com.semanticspace.neo4j;

import java.util.Map;
import java.util.stream.Stream;
import javax.annotation.processing.Generated;
import org.neo4j.gds.BaseProc;
import org.neo4j.gds.GraphAlgorithmFactory;
import org.neo4j.gds.api.Graph;
import org.neo4j.gds.api.NodeProperties;
import org.neo4j.gds.beta.pregel.Pregel;
import org.neo4j.gds.core.CypherMapWrapper;
import org.neo4j.gds.core.utils.mem.MemoryEstimation;
import org.neo4j.gds.core.utils.progress.tasks.ProgressTracker;
import org.neo4j.gds.core.utils.progress.tasks.Task;
import org.neo4j.gds.executor.ExecutionMode;
import org.neo4j.gds.executor.GdsCallable;
import org.neo4j.gds.pregel.proc.PregelStreamProc;
import org.neo4j.gds.pregel.proc.PregelStreamResult;
import org.neo4j.gds.results.MemoryEstimateResult;
import org.neo4j.procedure.Description;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

@GdsCallable(
        name = "semanticspace.gds.pregel.dijkstraMultiplePairs.stream",
        executionMode = ExecutionMode.STREAM
)
@Generated("org.neo4j.gds.beta.pregel.PregelProcessor")
public final class DijkstraMultipleInputsStreamProc extends PregelStreamProc<DijkstraMultipleInputsAlgorithm, MyPregelConfig> {
    @Procedure(
            name = "semanticspace.gds.pregel.dijkstraMultiplePairs.stream",
            mode = Mode.READ
    )
    public Stream<PregelStreamResult> stream(@Name("graphName") String graphName,
            @Name(value = "configuration", defaultValue = "{}") Map<String, Object> configuration) {
        return stream(compute(graphName, configuration));
    }

    @Procedure(
            name = "semanticspace.gds.pregel.dijkstraMultiplePairs.stream.estimate",
            mode = Mode.READ
    )
    @Description(BaseProc.ESTIMATE_DESCRIPTION)
    public Stream<MemoryEstimateResult> estimate(
            @Name("graphNameOrConfiguration") Object graphNameOrConfiguration,
            @Name("algoConfiguration") Map<String, Object> algoConfiguration) {
        return computeEstimate(graphNameOrConfiguration, algoConfiguration);
    }

    @Override
    protected PregelStreamResult streamResult(long originalNodeId, long internalNodeId,
            NodeProperties nodeProperties) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected MyPregelConfig newConfig(String username, CypherMapWrapper config) {
        return MyPregelConfig.of(config);
    }

    @Override
    public GraphAlgorithmFactory<DijkstraMultipleInputsAlgorithm, MyPregelConfig> algorithmFactory(
            ) {
        return new GraphAlgorithmFactory<DijkstraMultipleInputsAlgorithm, MyPregelConfig>() {
            @Override
            public DijkstraMultipleInputsAlgorithm build(Graph graph, MyPregelConfig configuration,
                    ProgressTracker progressTracker) {
                return new DijkstraMultipleInputsAlgorithm(graph, configuration, progressTracker);
            }

            @Override
            public String taskName() {
                return DijkstraMultipleInputsAlgorithm.class.getSimpleName();
            }

            @Override
            public Task progressTask(Graph graph, MyPregelConfig configuration) {
                return Pregel.progressTask(graph, configuration);
            }

            @Override
            public MemoryEstimation memoryEstimation(MyPregelConfig configuration) {
                var computation = new DijkstraMultipleInputs();
                return Pregel.memoryEstimation(computation.schema(configuration), computation.reducer().isEmpty(), configuration.isAsynchronous());
            }
        };
    }
}
