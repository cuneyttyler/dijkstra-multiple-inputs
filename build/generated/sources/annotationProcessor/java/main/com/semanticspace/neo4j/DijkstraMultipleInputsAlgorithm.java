package com.semanticspace.neo4j;

import javax.annotation.processing.Generated;
import org.neo4j.gds.Algorithm;
import org.neo4j.gds.api.Graph;
import org.neo4j.gds.beta.pregel.Pregel;
import org.neo4j.gds.beta.pregel.PregelResult;
import org.neo4j.gds.core.concurrency.Pools;
import org.neo4j.gds.core.utils.progress.tasks.ProgressTracker;

@Generated("org.neo4j.gds.beta.pregel.PregelProcessor")
public final class DijkstraMultipleInputsAlgorithm extends Algorithm<PregelResult> {
    private final Pregel<MyPregelConfig> pregelJob;

    DijkstraMultipleInputsAlgorithm(Graph graph, MyPregelConfig configuration,
            ProgressTracker progressTracker) {
        super(progressTracker);
        this.pregelJob = Pregel.create(graph, configuration, new DijkstraMultipleInputs(), Pools.DEFAULT, progressTracker);
    }

    @Override
    public PregelResult compute() {
        return pregelJob.run();
    }

    @Override
    public void release() {
        pregelJob.release();
    }
}
