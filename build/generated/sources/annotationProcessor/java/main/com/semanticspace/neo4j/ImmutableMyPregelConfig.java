package com.semanticspace.neo4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.immutables.value.Generated;
import org.jetbrains.annotations.Nullable;
import org.neo4j.gds.beta.pregel.Partitioning;
import org.neo4j.gds.beta.pregel.PregelConfig;
import org.neo4j.gds.beta.pregel.PregelProcedureConfig;
import org.neo4j.gds.config.AlgoBaseConfig;
import org.neo4j.gds.config.BaseConfig;
import org.neo4j.gds.config.ConcurrencyConfig;
import org.neo4j.gds.config.IterationsConfig;
import org.neo4j.gds.config.MutatePropertyConfig;
import org.neo4j.gds.config.RelationshipWeightConfig;
import org.neo4j.gds.config.WriteConfig;
import org.neo4j.gds.config.WritePropertyConfig;

/**
 * Immutable implementation of {@link MyPregelConfig}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableMyPregelConfig.builder()}.
 * Use the static factory method to create immutable instances:
 * {@code ImmutableMyPregelConfig.of()}.
 */
@Generated(from = "MyPregelConfig", generator = "Immutables")
@SuppressWarnings({"all"})
@javax.annotation.processing.Generated("org.immutables.processor.ProxyProcessor")
public final class ImmutableMyPregelConfig implements MyPregelConfig {
  private final @Nullable String usernameOverride;
  private final boolean sudo;
  private final Collection<String> configKeys;
  private transient final Map<String, Object> toMap;
  private final int concurrency;
  private final int minBatchSize;
  private final List<String> relationshipTypes;
  private final List<String> nodeLabels;
  private final @Nullable String relationshipWeightProperty;
  private transient final boolean hasRelationshipWeightProperty;
  private final int maxIterations;
  private final boolean isAsynchronous;
  private final Partitioning partitioning;
  private transient final boolean useForkJoin;
  private final int writeConcurrency;
  private final String writeProperty;
  private final String mutateProperty;
  private final long startNode;
  private final long endNode;

  private ImmutableMyPregelConfig(
      int concurrency,
      int minBatchSize,
      Iterable<String> relationshipTypes,
      Iterable<String> nodeLabels,
      @Nullable String relationshipWeightProperty,
      int maxIterations,
      boolean isAsynchronous,
      Partitioning partitioning,
      int writeConcurrency,
      String writeProperty,
      String mutateProperty,
      long startNode,
      long endNode) {
    initShim.concurrency(concurrency);
    initShim.minBatchSize(minBatchSize);
    initShim.relationshipTypes(createUnmodifiableList(false, createSafeList(relationshipTypes, true, false)));
    initShim.nodeLabels(createUnmodifiableList(false, createSafeList(nodeLabels, true, false)));
    initShim.relationshipWeightProperty(relationshipWeightProperty);
    this.maxIterations = maxIterations;
    initShim.isAsynchronous(isAsynchronous);
    initShim.partitioning(Objects.requireNonNull(partitioning, "partitioning"));
    initShim.writeConcurrency(writeConcurrency);
    initShim.writeProperty(Objects.requireNonNull(writeProperty, "writeProperty"));
    initShim.mutateProperty(Objects.requireNonNull(mutateProperty, "mutateProperty"));
    this.startNode = startNode;
    this.endNode = endNode;
    this.usernameOverride = initShim.usernameOverride();
    this.sudo = initShim.sudo();
    this.configKeys = initShim.configKeys();
    this.toMap = initShim.toMap();
    this.concurrency = initShim.concurrency();
    this.minBatchSize = initShim.minBatchSize();
    this.relationshipTypes = initShim.relationshipTypes();
    this.nodeLabels = initShim.nodeLabels();
    this.relationshipWeightProperty = initShim.relationshipWeightProperty();
    this.hasRelationshipWeightProperty = initShim.hasRelationshipWeightProperty();
    this.isAsynchronous = initShim.isAsynchronous();
    this.partitioning = initShim.partitioning();
    this.useForkJoin = initShim.useForkJoin();
    this.writeConcurrency = initShim.writeConcurrency();
    this.writeProperty = initShim.writeProperty();
    this.mutateProperty = initShim.mutateProperty();
    this.initShim = null;
  }

  private ImmutableMyPregelConfig(ImmutableMyPregelConfig.Builder builder) {
    this.maxIterations = builder.maxIterations;
    this.startNode = builder.startNode;
    this.endNode = builder.endNode;
    if (builder.usernameOverrideIsSet()) {
      initShim.usernameOverride(builder.usernameOverride);
    }
    if (builder.sudoIsSet()) {
      initShim.sudo(builder.sudo);
    }
    if (builder.configKeys != null) {
      initShim.configKeys(builder.configKeys);
    }
    if (builder.concurrencyIsSet()) {
      initShim.concurrency(builder.concurrency);
    }
    if (builder.minBatchSizeIsSet()) {
      initShim.minBatchSize(builder.minBatchSize);
    }
    if (builder.relationshipTypesIsSet()) {
      initShim.relationshipTypes(builder.relationshipTypes == null ? Collections.<String>emptyList() : createUnmodifiableList(true, builder.relationshipTypes));
    }
    if (builder.nodeLabelsIsSet()) {
      initShim.nodeLabels(builder.nodeLabels == null ? Collections.<String>emptyList() : createUnmodifiableList(true, builder.nodeLabels));
    }
    if (builder.relationshipWeightPropertyIsSet()) {
      initShim.relationshipWeightProperty(builder.relationshipWeightProperty);
    }
    if (builder.isAsynchronousIsSet()) {
      initShim.isAsynchronous(builder.isAsynchronous);
    }
    if (builder.partitioning != null) {
      initShim.partitioning(builder.partitioning);
    }
    if (builder.writeConcurrencyIsSet()) {
      initShim.writeConcurrency(builder.writeConcurrency);
    }
    if (builder.writeProperty != null) {
      initShim.writeProperty(builder.writeProperty);
    }
    if (builder.mutateProperty != null) {
      initShim.mutateProperty(builder.mutateProperty);
    }
    this.usernameOverride = initShim.usernameOverride();
    this.sudo = initShim.sudo();
    this.configKeys = initShim.configKeys();
    this.toMap = initShim.toMap();
    this.concurrency = initShim.concurrency();
    this.minBatchSize = initShim.minBatchSize();
    this.relationshipTypes = initShim.relationshipTypes();
    this.nodeLabels = initShim.nodeLabels();
    this.relationshipWeightProperty = initShim.relationshipWeightProperty();
    this.hasRelationshipWeightProperty = initShim.hasRelationshipWeightProperty();
    this.isAsynchronous = initShim.isAsynchronous();
    this.partitioning = initShim.partitioning();
    this.useForkJoin = initShim.useForkJoin();
    this.writeConcurrency = initShim.writeConcurrency();
    this.writeProperty = initShim.writeProperty();
    this.mutateProperty = initShim.mutateProperty();
    this.initShim = null;
  }

  private ImmutableMyPregelConfig(
      @Nullable String usernameOverride,
      boolean sudo,
      Collection<String> configKeys,
      int concurrency,
      int minBatchSize,
      List<String> relationshipTypes,
      List<String> nodeLabels,
      @Nullable String relationshipWeightProperty,
      int maxIterations,
      boolean isAsynchronous,
      Partitioning partitioning,
      int writeConcurrency,
      String writeProperty,
      String mutateProperty,
      long startNode,
      long endNode) {
    initShim.usernameOverride(usernameOverride);
    initShim.sudo(sudo);
    initShim.configKeys(configKeys);
    initShim.concurrency(concurrency);
    initShim.minBatchSize(minBatchSize);
    initShim.relationshipTypes(relationshipTypes);
    initShim.nodeLabels(nodeLabels);
    initShim.relationshipWeightProperty(relationshipWeightProperty);
    this.maxIterations = maxIterations;
    initShim.isAsynchronous(isAsynchronous);
    initShim.partitioning(partitioning);
    initShim.writeConcurrency(writeConcurrency);
    initShim.writeProperty(writeProperty);
    initShim.mutateProperty(mutateProperty);
    this.startNode = startNode;
    this.endNode = endNode;
    this.usernameOverride = initShim.usernameOverride();
    this.sudo = initShim.sudo();
    this.configKeys = initShim.configKeys();
    this.toMap = initShim.toMap();
    this.concurrency = initShim.concurrency();
    this.minBatchSize = initShim.minBatchSize();
    this.relationshipTypes = initShim.relationshipTypes();
    this.nodeLabels = initShim.nodeLabels();
    this.relationshipWeightProperty = initShim.relationshipWeightProperty();
    this.hasRelationshipWeightProperty = initShim.hasRelationshipWeightProperty();
    this.isAsynchronous = initShim.isAsynchronous();
    this.partitioning = initShim.partitioning();
    this.useForkJoin = initShim.useForkJoin();
    this.writeConcurrency = initShim.writeConcurrency();
    this.writeProperty = initShim.writeProperty();
    this.mutateProperty = initShim.mutateProperty();
    this.initShim = null;
  }

  private static final byte STAGE_INITIALIZING = -1;
  private static final byte STAGE_UNINITIALIZED = 0;
  private static final byte STAGE_INITIALIZED = 1;
  private transient volatile InitShim initShim = new InitShim();

  @Generated(from = "MyPregelConfig", generator = "Immutables")
  private final class InitShim {
    private byte usernameOverrideBuildStage = STAGE_UNINITIALIZED;
    private String usernameOverride;

    String usernameOverride() {
      if (usernameOverrideBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (usernameOverrideBuildStage == STAGE_UNINITIALIZED) {
        usernameOverrideBuildStage = STAGE_INITIALIZING;
        this.usernameOverride = usernameOverrideInitialize();
        usernameOverrideBuildStage = STAGE_INITIALIZED;
      }
      return this.usernameOverride;
    }

    void usernameOverride(String usernameOverride) {
      this.usernameOverride = usernameOverride;
      usernameOverrideBuildStage = STAGE_INITIALIZED;
    }

    private byte sudoBuildStage = STAGE_UNINITIALIZED;
    private boolean sudo;

    boolean sudo() {
      if (sudoBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (sudoBuildStage == STAGE_UNINITIALIZED) {
        sudoBuildStage = STAGE_INITIALIZING;
        this.sudo = sudoInitialize();
        sudoBuildStage = STAGE_INITIALIZED;
      }
      return this.sudo;
    }

    void sudo(boolean sudo) {
      this.sudo = sudo;
      sudoBuildStage = STAGE_INITIALIZED;
    }

    private byte configKeysBuildStage = STAGE_UNINITIALIZED;
    private Collection<String> configKeys;

    Collection<String> configKeys() {
      if (configKeysBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (configKeysBuildStage == STAGE_UNINITIALIZED) {
        configKeysBuildStage = STAGE_INITIALIZING;
        this.configKeys = Objects.requireNonNull(configKeysInitialize(), "configKeys");
        configKeysBuildStage = STAGE_INITIALIZED;
      }
      return this.configKeys;
    }

    void configKeys(Collection<String> configKeys) {
      this.configKeys = configKeys;
      configKeysBuildStage = STAGE_INITIALIZED;
    }

    private byte toMapBuildStage = STAGE_UNINITIALIZED;
    private Map<String, Object> toMap;

    Map<String, Object> toMap() {
      if (toMapBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (toMapBuildStage == STAGE_UNINITIALIZED) {
        toMapBuildStage = STAGE_INITIALIZING;
        this.toMap = Objects.requireNonNull(toMapInitialize(), "toMap");
        toMapBuildStage = STAGE_INITIALIZED;
      }
      return this.toMap;
    }

    private byte concurrencyBuildStage = STAGE_UNINITIALIZED;
    private int concurrency;

    int concurrency() {
      if (concurrencyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (concurrencyBuildStage == STAGE_UNINITIALIZED) {
        concurrencyBuildStage = STAGE_INITIALIZING;
        this.concurrency = concurrencyInitialize();
        concurrencyBuildStage = STAGE_INITIALIZED;
      }
      return this.concurrency;
    }

    void concurrency(int concurrency) {
      this.concurrency = concurrency;
      concurrencyBuildStage = STAGE_INITIALIZED;
    }

    private byte minBatchSizeBuildStage = STAGE_UNINITIALIZED;
    private int minBatchSize;

    int minBatchSize() {
      if (minBatchSizeBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (minBatchSizeBuildStage == STAGE_UNINITIALIZED) {
        minBatchSizeBuildStage = STAGE_INITIALIZING;
        this.minBatchSize = minBatchSizeInitialize();
        minBatchSizeBuildStage = STAGE_INITIALIZED;
      }
      return this.minBatchSize;
    }

    void minBatchSize(int minBatchSize) {
      this.minBatchSize = minBatchSize;
      minBatchSizeBuildStage = STAGE_INITIALIZED;
    }

    private byte relationshipTypesBuildStage = STAGE_UNINITIALIZED;
    private List<String> relationshipTypes;

    List<String> relationshipTypes() {
      if (relationshipTypesBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (relationshipTypesBuildStage == STAGE_UNINITIALIZED) {
        relationshipTypesBuildStage = STAGE_INITIALIZING;
        this.relationshipTypes = createUnmodifiableList(false, createSafeList(relationshipTypesInitialize(), true, false));
        relationshipTypesBuildStage = STAGE_INITIALIZED;
      }
      return this.relationshipTypes;
    }

    void relationshipTypes(List<String> relationshipTypes) {
      this.relationshipTypes = relationshipTypes;
      relationshipTypesBuildStage = STAGE_INITIALIZED;
    }

    private byte nodeLabelsBuildStage = STAGE_UNINITIALIZED;
    private List<String> nodeLabels;

    List<String> nodeLabels() {
      if (nodeLabelsBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (nodeLabelsBuildStage == STAGE_UNINITIALIZED) {
        nodeLabelsBuildStage = STAGE_INITIALIZING;
        this.nodeLabels = createUnmodifiableList(false, createSafeList(nodeLabelsInitialize(), true, false));
        nodeLabelsBuildStage = STAGE_INITIALIZED;
      }
      return this.nodeLabels;
    }

    void nodeLabels(List<String> nodeLabels) {
      this.nodeLabels = nodeLabels;
      nodeLabelsBuildStage = STAGE_INITIALIZED;
    }

    private byte relationshipWeightPropertyBuildStage = STAGE_UNINITIALIZED;
    private String relationshipWeightProperty;

    String relationshipWeightProperty() {
      if (relationshipWeightPropertyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (relationshipWeightPropertyBuildStage == STAGE_UNINITIALIZED) {
        relationshipWeightPropertyBuildStage = STAGE_INITIALIZING;
        this.relationshipWeightProperty = relationshipWeightPropertyInitialize();
        relationshipWeightPropertyBuildStage = STAGE_INITIALIZED;
      }
      return this.relationshipWeightProperty;
    }

    void relationshipWeightProperty(String relationshipWeightProperty) {
      this.relationshipWeightProperty = relationshipWeightProperty;
      relationshipWeightPropertyBuildStage = STAGE_INITIALIZED;
    }

    private byte hasRelationshipWeightPropertyBuildStage = STAGE_UNINITIALIZED;
    private boolean hasRelationshipWeightProperty;

    boolean hasRelationshipWeightProperty() {
      if (hasRelationshipWeightPropertyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (hasRelationshipWeightPropertyBuildStage == STAGE_UNINITIALIZED) {
        hasRelationshipWeightPropertyBuildStage = STAGE_INITIALIZING;
        this.hasRelationshipWeightProperty = hasRelationshipWeightPropertyInitialize();
        hasRelationshipWeightPropertyBuildStage = STAGE_INITIALIZED;
      }
      return this.hasRelationshipWeightProperty;
    }

    private byte isAsynchronousBuildStage = STAGE_UNINITIALIZED;
    private boolean isAsynchronous;

    boolean isAsynchronous() {
      if (isAsynchronousBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (isAsynchronousBuildStage == STAGE_UNINITIALIZED) {
        isAsynchronousBuildStage = STAGE_INITIALIZING;
        this.isAsynchronous = isAsynchronousInitialize();
        isAsynchronousBuildStage = STAGE_INITIALIZED;
      }
      return this.isAsynchronous;
    }

    void isAsynchronous(boolean isAsynchronous) {
      this.isAsynchronous = isAsynchronous;
      isAsynchronousBuildStage = STAGE_INITIALIZED;
    }

    private byte partitioningBuildStage = STAGE_UNINITIALIZED;
    private Partitioning partitioning;

    Partitioning partitioning() {
      if (partitioningBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (partitioningBuildStage == STAGE_UNINITIALIZED) {
        partitioningBuildStage = STAGE_INITIALIZING;
        this.partitioning = Objects.requireNonNull(partitioningInitialize(), "partitioning");
        partitioningBuildStage = STAGE_INITIALIZED;
      }
      return this.partitioning;
    }

    void partitioning(Partitioning partitioning) {
      this.partitioning = partitioning;
      partitioningBuildStage = STAGE_INITIALIZED;
    }

    private byte useForkJoinBuildStage = STAGE_UNINITIALIZED;
    private boolean useForkJoin;

    boolean useForkJoin() {
      if (useForkJoinBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (useForkJoinBuildStage == STAGE_UNINITIALIZED) {
        useForkJoinBuildStage = STAGE_INITIALIZING;
        this.useForkJoin = useForkJoinInitialize();
        useForkJoinBuildStage = STAGE_INITIALIZED;
      }
      return this.useForkJoin;
    }

    private byte writeConcurrencyBuildStage = STAGE_UNINITIALIZED;
    private int writeConcurrency;

    int writeConcurrency() {
      if (writeConcurrencyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (writeConcurrencyBuildStage == STAGE_UNINITIALIZED) {
        writeConcurrencyBuildStage = STAGE_INITIALIZING;
        this.writeConcurrency = writeConcurrencyInitialize();
        writeConcurrencyBuildStage = STAGE_INITIALIZED;
      }
      return this.writeConcurrency;
    }

    void writeConcurrency(int writeConcurrency) {
      this.writeConcurrency = writeConcurrency;
      writeConcurrencyBuildStage = STAGE_INITIALIZED;
    }

    private byte writePropertyBuildStage = STAGE_UNINITIALIZED;
    private String writeProperty;

    String writeProperty() {
      if (writePropertyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (writePropertyBuildStage == STAGE_UNINITIALIZED) {
        writePropertyBuildStage = STAGE_INITIALIZING;
        this.writeProperty = Objects.requireNonNull(writePropertyInitialize(), "writeProperty");
        writePropertyBuildStage = STAGE_INITIALIZED;
      }
      return this.writeProperty;
    }

    void writeProperty(String writeProperty) {
      this.writeProperty = writeProperty;
      writePropertyBuildStage = STAGE_INITIALIZED;
    }

    private byte mutatePropertyBuildStage = STAGE_UNINITIALIZED;
    private String mutateProperty;

    String mutateProperty() {
      if (mutatePropertyBuildStage == STAGE_INITIALIZING) throw new IllegalStateException(formatInitCycleMessage());
      if (mutatePropertyBuildStage == STAGE_UNINITIALIZED) {
        mutatePropertyBuildStage = STAGE_INITIALIZING;
        this.mutateProperty = Objects.requireNonNull(mutatePropertyInitialize(), "mutateProperty");
        mutatePropertyBuildStage = STAGE_INITIALIZED;
      }
      return this.mutateProperty;
    }

    void mutateProperty(String mutateProperty) {
      this.mutateProperty = mutateProperty;
      mutatePropertyBuildStage = STAGE_INITIALIZED;
    }

    private String formatInitCycleMessage() {
      List<String> attributes = new ArrayList<>();
      if (usernameOverrideBuildStage == STAGE_INITIALIZING) attributes.add("usernameOverride");
      if (sudoBuildStage == STAGE_INITIALIZING) attributes.add("sudo");
      if (configKeysBuildStage == STAGE_INITIALIZING) attributes.add("configKeys");
      if (toMapBuildStage == STAGE_INITIALIZING) attributes.add("toMap");
      if (concurrencyBuildStage == STAGE_INITIALIZING) attributes.add("concurrency");
      if (minBatchSizeBuildStage == STAGE_INITIALIZING) attributes.add("minBatchSize");
      if (relationshipTypesBuildStage == STAGE_INITIALIZING) attributes.add("relationshipTypes");
      if (nodeLabelsBuildStage == STAGE_INITIALIZING) attributes.add("nodeLabels");
      if (relationshipWeightPropertyBuildStage == STAGE_INITIALIZING) attributes.add("relationshipWeightProperty");
      if (hasRelationshipWeightPropertyBuildStage == STAGE_INITIALIZING) attributes.add("hasRelationshipWeightProperty");
      if (isAsynchronousBuildStage == STAGE_INITIALIZING) attributes.add("isAsynchronous");
      if (partitioningBuildStage == STAGE_INITIALIZING) attributes.add("partitioning");
      if (useForkJoinBuildStage == STAGE_INITIALIZING) attributes.add("useForkJoin");
      if (writeConcurrencyBuildStage == STAGE_INITIALIZING) attributes.add("writeConcurrency");
      if (writePropertyBuildStage == STAGE_INITIALIZING) attributes.add("writeProperty");
      if (mutatePropertyBuildStage == STAGE_INITIALIZING) attributes.add("mutateProperty");
      return "Cannot build MyPregelConfig, attribute initializers form cycle " + attributes;
    }
  }

  private @Nullable String usernameOverrideInitialize() {
    return MyPregelConfig.super.usernameOverride();
  }

  private boolean sudoInitialize() {
    return MyPregelConfig.super.sudo();
  }

  private Collection<String> configKeysInitialize() {
    return MyPregelConfig.super.configKeys();
  }

  private Map<String, Object> toMapInitialize() {
    return MyPregelConfig.super.toMap();
  }

  private int concurrencyInitialize() {
    return MyPregelConfig.super.concurrency();
  }

  private int minBatchSizeInitialize() {
    return MyPregelConfig.super.minBatchSize();
  }

  private List<String> relationshipTypesInitialize() {
    return MyPregelConfig.super.relationshipTypes();
  }

  private List<String> nodeLabelsInitialize() {
    return MyPregelConfig.super.nodeLabels();
  }

  private @Nullable String relationshipWeightPropertyInitialize() {
    return MyPregelConfig.super.relationshipWeightProperty();
  }

  private boolean hasRelationshipWeightPropertyInitialize() {
    return MyPregelConfig.super.hasRelationshipWeightProperty();
  }

  private boolean isAsynchronousInitialize() {
    return MyPregelConfig.super.isAsynchronous();
  }

  private Partitioning partitioningInitialize() {
    return MyPregelConfig.super.partitioning();
  }

  private boolean useForkJoinInitialize() {
    return MyPregelConfig.super.useForkJoin();
  }

  private int writeConcurrencyInitialize() {
    return MyPregelConfig.super.writeConcurrency();
  }

  private String writePropertyInitialize() {
    return MyPregelConfig.super.writeProperty();
  }

  private String mutatePropertyInitialize() {
    return MyPregelConfig.super.mutateProperty();
  }

  /**
   * @return The value of the {@code usernameOverride} attribute
   */
  @Override
  public @Nullable String usernameOverride() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.usernameOverride()
        : this.usernameOverride;
  }

  /**
   * @return The value of the {@code sudo} attribute
   */
  @Override
  public boolean sudo() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.sudo()
        : this.sudo;
  }

  /**
   * @return The value of the {@code configKeys} attribute
   */
  @Override
  public Collection<String> configKeys() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.configKeys()
        : this.configKeys;
  }

  /**
   * @return The computed-at-construction value of the {@code toMap} attribute
   */
  @Override
  public Map<String, Object> toMap() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.toMap()
        : this.toMap;
  }

  /**
   * @return The value of the {@code concurrency} attribute
   */
  @Override
  public int concurrency() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.concurrency()
        : this.concurrency;
  }

  /**
   * @return The value of the {@code minBatchSize} attribute
   */
  @Override
  public int minBatchSize() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.minBatchSize()
        : this.minBatchSize;
  }

  /**
   * @return The value of the {@code relationshipTypes} attribute
   */
  @Override
  public List<String> relationshipTypes() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.relationshipTypes()
        : this.relationshipTypes;
  }

  /**
   * @return The value of the {@code nodeLabels} attribute
   */
  @Override
  public List<String> nodeLabels() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.nodeLabels()
        : this.nodeLabels;
  }

  /**
   * @return The value of the {@code relationshipWeightProperty} attribute
   */
  @Override
  public @Nullable String relationshipWeightProperty() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.relationshipWeightProperty()
        : this.relationshipWeightProperty;
  }

  /**
   * @return The computed-at-construction value of the {@code hasRelationshipWeightProperty} attribute
   */
  @Override
  public boolean hasRelationshipWeightProperty() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.hasRelationshipWeightProperty()
        : this.hasRelationshipWeightProperty;
  }

  /**
   * @return The value of the {@code maxIterations} attribute
   */
  @Override
  public int maxIterations() {
    return maxIterations;
  }

  /**
   * @return The value of the {@code isAsynchronous} attribute
   */
  @Override
  public boolean isAsynchronous() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.isAsynchronous()
        : this.isAsynchronous;
  }

  /**
   * @return The value of the {@code partitioning} attribute
   */
  @Override
  public Partitioning partitioning() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.partitioning()
        : this.partitioning;
  }

  /**
   * @return The computed-at-construction value of the {@code useForkJoin} attribute
   */
  @Override
  public boolean useForkJoin() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.useForkJoin()
        : this.useForkJoin;
  }

  /**
   * @return The value of the {@code writeConcurrency} attribute
   */
  @Override
  public int writeConcurrency() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.writeConcurrency()
        : this.writeConcurrency;
  }

  /**
   * @return The value of the {@code writeProperty} attribute
   */
  @Override
  public String writeProperty() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.writeProperty()
        : this.writeProperty;
  }

  /**
   * @return The value of the {@code mutateProperty} attribute
   */
  @Override
  public String mutateProperty() {
    InitShim shim = this.initShim;
    return shim != null
        ? shim.mutateProperty()
        : this.mutateProperty;
  }

  /**
   * @return The value of the {@code startNode} attribute
   */
  @Override
  public long startNode() {
    return startNode;
  }

  /**
   * @return The value of the {@code endNode} attribute
   */
  @Override
  public long endNode() {
    return endNode;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#usernameOverride() usernameOverride} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for usernameOverride (can be {@code null})
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withUsernameOverride(@Nullable String value) {
    if (Objects.equals(this.usernameOverride, value)) return this;
    return validate(new ImmutableMyPregelConfig(
        value,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#sudo() sudo} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for sudo
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withSudo(boolean value) {
    if (this.sudo == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        value,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#configKeys() configKeys} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for configKeys
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withConfigKeys(Collection<String> value) {
    if (this.configKeys == value) return this;
    Collection<String> newValue = Objects.requireNonNull(value, "configKeys");
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        newValue,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#concurrency() concurrency} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for concurrency
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withConcurrency(int value) {
    if (this.concurrency == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        value,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#minBatchSize() minBatchSize} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for minBatchSize
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withMinBatchSize(int value) {
    if (this.minBatchSize == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        value,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link MyPregelConfig#relationshipTypes() relationshipTypes}.
   * @param elements The elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutableMyPregelConfig withRelationshipTypes(String... elements) {
    List<String> newValue = createUnmodifiableList(false, createSafeList(Arrays.asList(elements), true, false));
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        newValue,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link MyPregelConfig#relationshipTypes() relationshipTypes}.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param elements An iterable of relationshipTypes elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutableMyPregelConfig withRelationshipTypes(Iterable<String> elements) {
    if (this.relationshipTypes == elements) return this;
    List<String> newValue = createUnmodifiableList(false, createSafeList(elements, true, false));
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        newValue,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link MyPregelConfig#nodeLabels() nodeLabels}.
   * @param elements The elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutableMyPregelConfig withNodeLabels(String... elements) {
    List<String> newValue = createUnmodifiableList(false, createSafeList(Arrays.asList(elements), true, false));
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        newValue,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object with elements that replace the content of {@link MyPregelConfig#nodeLabels() nodeLabels}.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param elements An iterable of nodeLabels elements to set
   * @return A modified copy of {@code this} object
   */
  public final ImmutableMyPregelConfig withNodeLabels(Iterable<String> elements) {
    if (this.nodeLabels == elements) return this;
    List<String> newValue = createUnmodifiableList(false, createSafeList(elements, true, false));
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        newValue,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#relationshipWeightProperty() relationshipWeightProperty} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for relationshipWeightProperty (can be {@code null})
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withRelationshipWeightProperty(@Nullable String value) {
    if (Objects.equals(this.relationshipWeightProperty, value)) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        value,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#maxIterations() maxIterations} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for maxIterations
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withMaxIterations(int value) {
    if (this.maxIterations == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        value,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#isAsynchronous() isAsynchronous} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for isAsynchronous
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withIsAsynchronous(boolean value) {
    if (this.isAsynchronous == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        value,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#partitioning() partitioning} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for partitioning
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withPartitioning(Partitioning value) {
    if (this.partitioning == value) return this;
    Partitioning newValue = Objects.requireNonNull(value, "partitioning");
    if (this.partitioning.equals(newValue)) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        newValue,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#writeConcurrency() writeConcurrency} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for writeConcurrency
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withWriteConcurrency(int value) {
    if (this.writeConcurrency == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        value,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#writeProperty() writeProperty} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for writeProperty
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withWriteProperty(String value) {
    String newValue = Objects.requireNonNull(value, "writeProperty");
    if (this.writeProperty.equals(newValue)) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        newValue,
        this.mutateProperty,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#mutateProperty() mutateProperty} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for mutateProperty
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withMutateProperty(String value) {
    String newValue = Objects.requireNonNull(value, "mutateProperty");
    if (this.mutateProperty.equals(newValue)) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        newValue,
        this.startNode,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#startNode() startNode} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for startNode
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withStartNode(long value) {
    if (this.startNode == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        value,
        this.endNode));
  }

  /**
   * Copy the current immutable object by setting a value for the {@link MyPregelConfig#endNode() endNode} attribute.
   * A value equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for endNode
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMyPregelConfig withEndNode(long value) {
    if (this.endNode == value) return this;
    return validate(new ImmutableMyPregelConfig(
        this.usernameOverride,
        this.sudo,
        this.configKeys,
        this.concurrency,
        this.minBatchSize,
        this.relationshipTypes,
        this.nodeLabels,
        this.relationshipWeightProperty,
        this.maxIterations,
        this.isAsynchronous,
        this.partitioning,
        this.writeConcurrency,
        this.writeProperty,
        this.mutateProperty,
        this.startNode,
        value));
  }

  /**
   * This instance is equal to all instances of {@code ImmutableMyPregelConfig} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(Object another) {
    if (this == another) return true;
    return another instanceof ImmutableMyPregelConfig
        && equalTo((ImmutableMyPregelConfig) another);
  }

  private boolean equalTo(ImmutableMyPregelConfig another) {
    return Objects.equals(usernameOverride, another.usernameOverride)
        && sudo == another.sudo
        && concurrency == another.concurrency
        && minBatchSize == another.minBatchSize
        && relationshipTypes.equals(another.relationshipTypes)
        && nodeLabels.equals(another.nodeLabels)
        && Objects.equals(relationshipWeightProperty, another.relationshipWeightProperty)
        && hasRelationshipWeightProperty == another.hasRelationshipWeightProperty
        && maxIterations == another.maxIterations
        && isAsynchronous == another.isAsynchronous
        && partitioning.equals(another.partitioning)
        && useForkJoin == another.useForkJoin
        && writeConcurrency == another.writeConcurrency
        && writeProperty.equals(another.writeProperty)
        && mutateProperty.equals(another.mutateProperty)
        && startNode == another.startNode
        && endNode == another.endNode;
  }

  /**
   * Computes a hash code from attributes: {@code usernameOverride}, {@code sudo}, {@code concurrency}, {@code minBatchSize}, {@code relationshipTypes}, {@code nodeLabels}, {@code relationshipWeightProperty}, {@code hasRelationshipWeightProperty}, {@code maxIterations}, {@code isAsynchronous}, {@code partitioning}, {@code useForkJoin}, {@code writeConcurrency}, {@code writeProperty}, {@code mutateProperty}, {@code startNode}, {@code endNode}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + Objects.hashCode(usernameOverride);
    h += (h << 5) + Boolean.hashCode(sudo);
    h += (h << 5) + concurrency;
    h += (h << 5) + minBatchSize;
    h += (h << 5) + relationshipTypes.hashCode();
    h += (h << 5) + nodeLabels.hashCode();
    h += (h << 5) + Objects.hashCode(relationshipWeightProperty);
    h += (h << 5) + Boolean.hashCode(hasRelationshipWeightProperty);
    h += (h << 5) + maxIterations;
    h += (h << 5) + Boolean.hashCode(isAsynchronous);
    h += (h << 5) + partitioning.hashCode();
    h += (h << 5) + Boolean.hashCode(useForkJoin);
    h += (h << 5) + writeConcurrency;
    h += (h << 5) + writeProperty.hashCode();
    h += (h << 5) + mutateProperty.hashCode();
    h += (h << 5) + Long.hashCode(startNode);
    h += (h << 5) + Long.hashCode(endNode);
    return h;
  }


  /**
   * Prints the immutable value {@code MyPregelConfig} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return "MyPregelConfig{"
        + "usernameOverride=" + usernameOverride
        + ", sudo=" + sudo
        + ", concurrency=" + concurrency
        + ", minBatchSize=" + minBatchSize
        + ", relationshipTypes=" + relationshipTypes
        + ", nodeLabels=" + nodeLabels
        + ", relationshipWeightProperty=" + relationshipWeightProperty
        + ", hasRelationshipWeightProperty=" + hasRelationshipWeightProperty
        + ", maxIterations=" + maxIterations
        + ", isAsynchronous=" + isAsynchronous
        + ", partitioning=" + partitioning
        + ", useForkJoin=" + useForkJoin
        + ", writeConcurrency=" + writeConcurrency
        + ", writeProperty=" + writeProperty
        + ", mutateProperty=" + mutateProperty
        + ", startNode=" + startNode
        + ", endNode=" + endNode
        + "}";
  }

  /**
   * Construct a new immutable {@code MyPregelConfig} instance.
   * @param concurrency The value for the {@code concurrency} attribute
   * @param minBatchSize The value for the {@code minBatchSize} attribute
   * @param relationshipTypes The value for the {@code relationshipTypes} attribute
   * @param nodeLabels The value for the {@code nodeLabels} attribute
   * @param relationshipWeightProperty The value for the {@code relationshipWeightProperty} attribute
   * @param maxIterations The value for the {@code maxIterations} attribute
   * @param isAsynchronous The value for the {@code isAsynchronous} attribute
   * @param partitioning The value for the {@code partitioning} attribute
   * @param writeConcurrency The value for the {@code writeConcurrency} attribute
   * @param writeProperty The value for the {@code writeProperty} attribute
   * @param mutateProperty The value for the {@code mutateProperty} attribute
   * @param startNode The value for the {@code startNode} attribute
   * @param endNode The value for the {@code endNode} attribute
   * @return An immutable MyPregelConfig instance
   */
  public static MyPregelConfig of(int concurrency, int minBatchSize, List<String> relationshipTypes, List<String> nodeLabels, @Nullable String relationshipWeightProperty, int maxIterations, boolean isAsynchronous, Partitioning partitioning, int writeConcurrency, String writeProperty, String mutateProperty, long startNode, long endNode) {
    return of(concurrency, minBatchSize, (Iterable<String>) relationshipTypes, (Iterable<String>) nodeLabels, relationshipWeightProperty, maxIterations, isAsynchronous, partitioning, writeConcurrency, writeProperty, mutateProperty, startNode, endNode);
  }

  /**
   * Construct a new immutable {@code MyPregelConfig} instance.
   * @param concurrency The value for the {@code concurrency} attribute
   * @param minBatchSize The value for the {@code minBatchSize} attribute
   * @param relationshipTypes The value for the {@code relationshipTypes} attribute
   * @param nodeLabels The value for the {@code nodeLabels} attribute
   * @param relationshipWeightProperty The value for the {@code relationshipWeightProperty} attribute
   * @param maxIterations The value for the {@code maxIterations} attribute
   * @param isAsynchronous The value for the {@code isAsynchronous} attribute
   * @param partitioning The value for the {@code partitioning} attribute
   * @param writeConcurrency The value for the {@code writeConcurrency} attribute
   * @param writeProperty The value for the {@code writeProperty} attribute
   * @param mutateProperty The value for the {@code mutateProperty} attribute
   * @param startNode The value for the {@code startNode} attribute
   * @param endNode The value for the {@code endNode} attribute
   * @return An immutable MyPregelConfig instance
   */
  public static MyPregelConfig of(int concurrency, int minBatchSize, Iterable<String> relationshipTypes, Iterable<String> nodeLabels, @Nullable String relationshipWeightProperty, int maxIterations, boolean isAsynchronous, Partitioning partitioning, int writeConcurrency, String writeProperty, String mutateProperty, long startNode, long endNode) {
    return validate(new ImmutableMyPregelConfig(concurrency, minBatchSize, relationshipTypes, nodeLabels, relationshipWeightProperty, maxIterations, isAsynchronous, partitioning, writeConcurrency, writeProperty, mutateProperty, startNode, endNode));
  }

  private static ImmutableMyPregelConfig validate(ImmutableMyPregelConfig instance) {
    instance.validateWriteConcurrency();
    instance.validateConcurrency();
    return instance;
  }

  /**
   * Creates an immutable copy of a {@link MyPregelConfig} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable MyPregelConfig instance
   */
  public static MyPregelConfig copyOf(MyPregelConfig instance) {
    if (instance instanceof ImmutableMyPregelConfig) {
      return (ImmutableMyPregelConfig) instance;
    }
    return ImmutableMyPregelConfig.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link MyPregelConfig MyPregelConfig}.
   * <pre>
   * ImmutableMyPregelConfig.builder()
   *    .usernameOverride(String | null) // nullable {@link MyPregelConfig#usernameOverride() usernameOverride}
   *    .sudo(boolean) // optional {@link MyPregelConfig#sudo() sudo}
   *    .configKeys(Collection&amp;lt;String&amp;gt;) // optional {@link MyPregelConfig#configKeys() configKeys}
   *    .concurrency(int) // optional {@link MyPregelConfig#concurrency() concurrency}
   *    .minBatchSize(int) // optional {@link MyPregelConfig#minBatchSize() minBatchSize}
   *    .addRelationshipType|addAllRelationshipTypes(String) // {@link MyPregelConfig#relationshipTypes() relationshipTypes} elements
   *    .addNodeLabel|addAllNodeLabels(String) // {@link MyPregelConfig#nodeLabels() nodeLabels} elements
   *    .relationshipWeightProperty(String | null) // nullable {@link MyPregelConfig#relationshipWeightProperty() relationshipWeightProperty}
   *    .maxIterations(int) // required {@link MyPregelConfig#maxIterations() maxIterations}
   *    .isAsynchronous(boolean) // optional {@link MyPregelConfig#isAsynchronous() isAsynchronous}
   *    .partitioning(org.neo4j.gds.beta.pregel.Partitioning) // optional {@link MyPregelConfig#partitioning() partitioning}
   *    .writeConcurrency(int) // optional {@link MyPregelConfig#writeConcurrency() writeConcurrency}
   *    .writeProperty(String) // optional {@link MyPregelConfig#writeProperty() writeProperty}
   *    .mutateProperty(String) // optional {@link MyPregelConfig#mutateProperty() mutateProperty}
   *    .startNode(long) // required {@link MyPregelConfig#startNode() startNode}
   *    .endNode(long) // required {@link MyPregelConfig#endNode() endNode}
   *    .build();
   * </pre>
   * @return A new MyPregelConfig builder
   */
  public static ImmutableMyPregelConfig.Builder builder() {
    return new ImmutableMyPregelConfig.Builder();
  }

  /**
   * Builds instances of type {@link MyPregelConfig MyPregelConfig}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @Generated(from = "MyPregelConfig", generator = "Immutables")
  public static final class Builder {
    private static final long INIT_BIT_MAX_ITERATIONS = 0x1L;
    private static final long INIT_BIT_START_NODE = 0x2L;
    private static final long INIT_BIT_END_NODE = 0x4L;
    private static final long OPT_BIT_USERNAME_OVERRIDE = 0x1L;
    private static final long OPT_BIT_SUDO = 0x2L;
    private static final long OPT_BIT_CONCURRENCY = 0x4L;
    private static final long OPT_BIT_MIN_BATCH_SIZE = 0x8L;
    private static final long OPT_BIT_RELATIONSHIP_TYPES = 0x10L;
    private static final long OPT_BIT_NODE_LABELS = 0x20L;
    private static final long OPT_BIT_RELATIONSHIP_WEIGHT_PROPERTY = 0x40L;
    private static final long OPT_BIT_IS_ASYNCHRONOUS = 0x80L;
    private static final long OPT_BIT_WRITE_CONCURRENCY = 0x100L;
    private long initBits = 0x7L;
    private long optBits;

    private String usernameOverride;
    private boolean sudo;
    private Collection<String> configKeys;
    private int concurrency;
    private int minBatchSize;
    private List<String> relationshipTypes = null;
    private List<String> nodeLabels = null;
    private String relationshipWeightProperty;
    private int maxIterations;
    private boolean isAsynchronous;
    private Partitioning partitioning;
    private int writeConcurrency;
    private String writeProperty;
    private String mutateProperty;
    private long startNode;
    private long endNode;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.RelationshipWeightConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(RelationshipWeightConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.WriteConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(WriteConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.WritePropertyConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(WritePropertyConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.ConcurrencyConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(ConcurrencyConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.beta.pregel.PregelProcedureConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(PregelProcedureConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.MutatePropertyConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(MutatePropertyConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code com.semanticspace.neo4j.MyPregelConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(MyPregelConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.BaseConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(BaseConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.beta.pregel.PregelConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(PregelConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.AlgoBaseConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(AlgoBaseConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    /**
     * Fill a builder with attribute values from the provided {@code org.neo4j.gds.config.IterationsConfig} instance.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(IterationsConfig instance) {
      Objects.requireNonNull(instance, "instance");
      from((Object) instance);
      return this;
    }

    private void from(Object object) {
      long bits = 0;
      if (object instanceof RelationshipWeightConfig) {
        RelationshipWeightConfig instance = (RelationshipWeightConfig) object;
        @Nullable String relationshipWeightPropertyValue = instance.relationshipWeightProperty();
        if (relationshipWeightPropertyValue != null) {
          relationshipWeightProperty(relationshipWeightPropertyValue);
        }
      }
      if (object instanceof WriteConfig) {
        WriteConfig instance = (WriteConfig) object;
        writeConcurrency(instance.writeConcurrency());
      }
      if (object instanceof WritePropertyConfig) {
        WritePropertyConfig instance = (WritePropertyConfig) object;
        if ((bits & 0x1L) == 0) {
          writeProperty(instance.writeProperty());
          bits |= 0x1L;
        }
      }
      if (object instanceof ConcurrencyConfig) {
        ConcurrencyConfig instance = (ConcurrencyConfig) object;
        minBatchSize(instance.minBatchSize());
        concurrency(instance.concurrency());
      }
      if (object instanceof PregelProcedureConfig) {
        PregelProcedureConfig instance = (PregelProcedureConfig) object;
        if ((bits & 0x1L) == 0) {
          writeProperty(instance.writeProperty());
          bits |= 0x1L;
        }
        if ((bits & 0x2L) == 0) {
          mutateProperty(instance.mutateProperty());
          bits |= 0x2L;
        }
      }
      if (object instanceof MutatePropertyConfig) {
        MutatePropertyConfig instance = (MutatePropertyConfig) object;
        if ((bits & 0x2L) == 0) {
          mutateProperty(instance.mutateProperty());
          bits |= 0x2L;
        }
      }
      if (object instanceof MyPregelConfig) {
        MyPregelConfig instance = (MyPregelConfig) object;
        endNode(instance.endNode());
        startNode(instance.startNode());
      }
      if (object instanceof BaseConfig) {
        BaseConfig instance = (BaseConfig) object;
        @Nullable String usernameOverrideValue = instance.usernameOverride();
        if (usernameOverrideValue != null) {
          usernameOverride(usernameOverrideValue);
        }
        configKeys(instance.configKeys());
        sudo(instance.sudo());
      }
      if (object instanceof PregelConfig) {
        PregelConfig instance = (PregelConfig) object;
        partitioning(instance.partitioning());
        isAsynchronous(instance.isAsynchronous());
      }
      if (object instanceof AlgoBaseConfig) {
        AlgoBaseConfig instance = (AlgoBaseConfig) object;
        addAllRelationshipTypes(instance.relationshipTypes());
        addAllNodeLabels(instance.nodeLabels());
      }
      if (object instanceof IterationsConfig) {
        IterationsConfig instance = (IterationsConfig) object;
        maxIterations(instance.maxIterations());
      }
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#usernameOverride() usernameOverride} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#usernameOverride() usernameOverride}.</em>
     * @param usernameOverride The value for usernameOverride (can be {@code null})
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder usernameOverride(@Nullable String usernameOverride) {
      this.usernameOverride = usernameOverride;
      optBits |= OPT_BIT_USERNAME_OVERRIDE;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#sudo() sudo} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#sudo() sudo}.</em>
     * @param sudo The value for sudo 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder sudo(boolean sudo) {
      this.sudo = sudo;
      optBits |= OPT_BIT_SUDO;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#configKeys() configKeys} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#configKeys() configKeys}.</em>
     * @param configKeys The value for configKeys 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder configKeys(Collection<String> configKeys) {
      this.configKeys = Objects.requireNonNull(configKeys, "configKeys");
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#concurrency() concurrency} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#concurrency() concurrency}.</em>
     * @param concurrency The value for concurrency 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder concurrency(int concurrency) {
      this.concurrency = concurrency;
      optBits |= OPT_BIT_CONCURRENCY;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#minBatchSize() minBatchSize} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#minBatchSize() minBatchSize}.</em>
     * @param minBatchSize The value for minBatchSize 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder minBatchSize(int minBatchSize) {
      this.minBatchSize = minBatchSize;
      optBits |= OPT_BIT_MIN_BATCH_SIZE;
      return this;
    }

    /**
     * Adds one element to {@link MyPregelConfig#relationshipTypes() relationshipTypes} list.
     * @param element A relationshipTypes element
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addRelationshipType(String element) {
      if (this.relationshipTypes == null) {
        this.relationshipTypes = new ArrayList<String>();
      }
      this.relationshipTypes.add(Objects.requireNonNull(element, "relationshipTypes element"));
      optBits |= OPT_BIT_RELATIONSHIP_TYPES;
      return this;
    }

    /**
     * Adds elements to {@link MyPregelConfig#relationshipTypes() relationshipTypes} list.
     * @param elements An array of relationshipTypes elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addRelationshipTypes(String... elements) {
      if (this.relationshipTypes == null) {
        this.relationshipTypes = new ArrayList<String>();
      }
      for (String element : elements) {
        this.relationshipTypes.add(Objects.requireNonNull(element, "relationshipTypes element"));
      }
      optBits |= OPT_BIT_RELATIONSHIP_TYPES;
      return this;
    }


    /**
     * Sets or replaces all elements for {@link MyPregelConfig#relationshipTypes() relationshipTypes} list.
     * @param elements An iterable of relationshipTypes elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder relationshipTypes(Iterable<String> elements) {
      this.relationshipTypes = new ArrayList<String>();
      return addAllRelationshipTypes(elements);
    }

    /**
     * Adds elements to {@link MyPregelConfig#relationshipTypes() relationshipTypes} list.
     * @param elements An iterable of relationshipTypes elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addAllRelationshipTypes(Iterable<String> elements) {
      Objects.requireNonNull(elements, "relationshipTypes element");
      if (this.relationshipTypes == null) {
        this.relationshipTypes = new ArrayList<String>();
      }
      for (String element : elements) {
        this.relationshipTypes.add(Objects.requireNonNull(element, "relationshipTypes element"));
      }
      optBits |= OPT_BIT_RELATIONSHIP_TYPES;
      return this;
    }

    /**
     * Adds one element to {@link MyPregelConfig#nodeLabels() nodeLabels} list.
     * @param element A nodeLabels element
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addNodeLabel(String element) {
      if (this.nodeLabels == null) {
        this.nodeLabels = new ArrayList<String>();
      }
      this.nodeLabels.add(Objects.requireNonNull(element, "nodeLabels element"));
      optBits |= OPT_BIT_NODE_LABELS;
      return this;
    }

    /**
     * Adds elements to {@link MyPregelConfig#nodeLabels() nodeLabels} list.
     * @param elements An array of nodeLabels elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addNodeLabels(String... elements) {
      if (this.nodeLabels == null) {
        this.nodeLabels = new ArrayList<String>();
      }
      for (String element : elements) {
        this.nodeLabels.add(Objects.requireNonNull(element, "nodeLabels element"));
      }
      optBits |= OPT_BIT_NODE_LABELS;
      return this;
    }


    /**
     * Sets or replaces all elements for {@link MyPregelConfig#nodeLabels() nodeLabels} list.
     * @param elements An iterable of nodeLabels elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder nodeLabels(Iterable<String> elements) {
      this.nodeLabels = new ArrayList<String>();
      return addAllNodeLabels(elements);
    }

    /**
     * Adds elements to {@link MyPregelConfig#nodeLabels() nodeLabels} list.
     * @param elements An iterable of nodeLabels elements
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder addAllNodeLabels(Iterable<String> elements) {
      Objects.requireNonNull(elements, "nodeLabels element");
      if (this.nodeLabels == null) {
        this.nodeLabels = new ArrayList<String>();
      }
      for (String element : elements) {
        this.nodeLabels.add(Objects.requireNonNull(element, "nodeLabels element"));
      }
      optBits |= OPT_BIT_NODE_LABELS;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#relationshipWeightProperty() relationshipWeightProperty} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#relationshipWeightProperty() relationshipWeightProperty}.</em>
     * @param relationshipWeightProperty The value for relationshipWeightProperty (can be {@code null})
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder relationshipWeightProperty(@Nullable String relationshipWeightProperty) {
      this.relationshipWeightProperty = relationshipWeightProperty;
      optBits |= OPT_BIT_RELATIONSHIP_WEIGHT_PROPERTY;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#maxIterations() maxIterations} attribute.
     * @param maxIterations The value for maxIterations 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder maxIterations(int maxIterations) {
      this.maxIterations = maxIterations;
      initBits &= ~INIT_BIT_MAX_ITERATIONS;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#isAsynchronous() isAsynchronous} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#isAsynchronous() isAsynchronous}.</em>
     * @param isAsynchronous The value for isAsynchronous 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder isAsynchronous(boolean isAsynchronous) {
      this.isAsynchronous = isAsynchronous;
      optBits |= OPT_BIT_IS_ASYNCHRONOUS;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#partitioning() partitioning} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#partitioning() partitioning}.</em>
     * @param partitioning The value for partitioning 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder partitioning(Partitioning partitioning) {
      this.partitioning = Objects.requireNonNull(partitioning, "partitioning");
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#writeConcurrency() writeConcurrency} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#writeConcurrency() writeConcurrency}.</em>
     * @param writeConcurrency The value for writeConcurrency 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder writeConcurrency(int writeConcurrency) {
      this.writeConcurrency = writeConcurrency;
      optBits |= OPT_BIT_WRITE_CONCURRENCY;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#writeProperty() writeProperty} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#writeProperty() writeProperty}.</em>
     * @param writeProperty The value for writeProperty 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder writeProperty(String writeProperty) {
      this.writeProperty = Objects.requireNonNull(writeProperty, "writeProperty");
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#mutateProperty() mutateProperty} attribute.
     * <p><em>If not set, this attribute will have a default value as returned by the initializer of {@link MyPregelConfig#mutateProperty() mutateProperty}.</em>
     * @param mutateProperty The value for mutateProperty 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder mutateProperty(String mutateProperty) {
      this.mutateProperty = Objects.requireNonNull(mutateProperty, "mutateProperty");
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#startNode() startNode} attribute.
     * @param startNode The value for startNode 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder startNode(long startNode) {
      this.startNode = startNode;
      initBits &= ~INIT_BIT_START_NODE;
      return this;
    }

    /**
     * Initializes the value for the {@link MyPregelConfig#endNode() endNode} attribute.
     * @param endNode The value for endNode 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder endNode(long endNode) {
      this.endNode = endNode;
      initBits &= ~INIT_BIT_END_NODE;
      return this;
    }

    /**
     * Clear the builder to the initial state.
     * @return {@code this} builder for use in a chained invocation
     */
    public Builder clear() {
      initBits = 0x7L;
      optBits = 0;
      this.usernameOverride = null;
      this.sudo = false;
      this.configKeys = null;
      this.concurrency = 0;
      this.minBatchSize = 0;
      if (relationshipTypes != null) {
        relationshipTypes.clear();
      }
      if (nodeLabels != null) {
        nodeLabels.clear();
      }
      this.relationshipWeightProperty = null;
      this.maxIterations = 0;
      this.isAsynchronous = false;
      this.partitioning = null;
      this.writeConcurrency = 0;
      this.writeProperty = null;
      this.mutateProperty = null;
      this.startNode = 0;
      this.endNode = 0;
      return this;
    }

    /**
     * Builds a new {@link MyPregelConfig MyPregelConfig}.
     * @return An immutable instance of MyPregelConfig
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public MyPregelConfig build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return ImmutableMyPregelConfig.validate(new ImmutableMyPregelConfig(this));
    }

    private boolean usernameOverrideIsSet() {
      return (optBits & OPT_BIT_USERNAME_OVERRIDE) != 0;
    }

    private boolean sudoIsSet() {
      return (optBits & OPT_BIT_SUDO) != 0;
    }

    private boolean concurrencyIsSet() {
      return (optBits & OPT_BIT_CONCURRENCY) != 0;
    }

    private boolean minBatchSizeIsSet() {
      return (optBits & OPT_BIT_MIN_BATCH_SIZE) != 0;
    }

    private boolean relationshipTypesIsSet() {
      return (optBits & OPT_BIT_RELATIONSHIP_TYPES) != 0;
    }

    private boolean nodeLabelsIsSet() {
      return (optBits & OPT_BIT_NODE_LABELS) != 0;
    }

    private boolean relationshipWeightPropertyIsSet() {
      return (optBits & OPT_BIT_RELATIONSHIP_WEIGHT_PROPERTY) != 0;
    }

    private boolean isAsynchronousIsSet() {
      return (optBits & OPT_BIT_IS_ASYNCHRONOUS) != 0;
    }

    private boolean writeConcurrencyIsSet() {
      return (optBits & OPT_BIT_WRITE_CONCURRENCY) != 0;
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if ((initBits & INIT_BIT_MAX_ITERATIONS) != 0) attributes.add("maxIterations");
      if ((initBits & INIT_BIT_START_NODE) != 0) attributes.add("startNode");
      if ((initBits & INIT_BIT_END_NODE) != 0) attributes.add("endNode");
      return "Cannot build MyPregelConfig, some of required attributes are not set " + attributes;
    }
  }

  private static <T> List<T> createSafeList(Iterable<? extends T> iterable, boolean checkNulls, boolean skipNulls) {
    ArrayList<T> list;
    if (iterable instanceof Collection<?>) {
      int size = ((Collection<?>) iterable).size();
      if (size == 0) return Collections.emptyList();
      list = new ArrayList<>();
    } else {
      list = new ArrayList<>();
    }
    for (T element : iterable) {
      if (skipNulls && element == null) continue;
      if (checkNulls) Objects.requireNonNull(element, "element");
      list.add(element);
    }
    return list;
  }

  private static <T> List<T> createUnmodifiableList(boolean clone, List<T> list) {
    switch(list.size()) {
    case 0: return Collections.emptyList();
    case 1: return Collections.singletonList(list.get(0));
    default:
      if (clone) {
        return Collections.unmodifiableList(new ArrayList<>(list));
      } else {
        if (list instanceof ArrayList<?>) {
          ((ArrayList<?>) list).trimToSize();
        }
        return Collections.unmodifiableList(list);
      }
    }
  }
}
