package com.semanticspace.neo4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.processing.Generated;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.neo4j.gds.NodeLabel;
import org.neo4j.gds.RelationshipType;
import org.neo4j.gds.api.GraphStore;
import org.neo4j.gds.beta.pregel.Partitioning;
import org.neo4j.gds.config.RelationshipWeightConfig;
import org.neo4j.gds.core.CypherMapWrapper;

@Generated("org.neo4j.gds.proc.ConfigurationProcessor")
public final class MyPregelConfigImpl implements MyPregelConfig {
    private long startNode;

    private long endNode;

    private String writeProperty;

    private String mutateProperty;

    private boolean isAsynchronous;

    private Partitioning partitioning;

    private List<String> relationshipTypes;

    private List<String> nodeLabels;

    private @Nullable String usernameOverride;

    private boolean sudo;

    private int concurrency;

    private @Nullable String relationshipWeightProperty;

    private int maxIterations;

    private int writeConcurrency;

    public MyPregelConfigImpl(@NotNull CypherMapWrapper config) {
        ArrayList<IllegalArgumentException> errors = new ArrayList<>();
        try {
            this.startNode = config.requireLong("startNode");
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.endNode = config.requireLong("endNode");
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.writeProperty = CypherMapWrapper.failOnNull("writeProperty", config.getString("writeProperty", MyPregelConfig.super.writeProperty()));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.mutateProperty = CypherMapWrapper.failOnNull("mutateProperty", config.getString("mutateProperty", MyPregelConfig.super.mutateProperty()));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.isAsynchronous = config.getBool("isAsynchronous", MyPregelConfig.super.isAsynchronous());
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.partitioning = CypherMapWrapper.failOnNull("partitioning", Partitioning.parse(config.getChecked("partitioning", MyPregelConfig.super.partitioning(), Object.class)));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.relationshipTypes = CypherMapWrapper.failOnNull("relationshipTypes", config.getChecked("relationshipTypes", MyPregelConfig.super.relationshipTypes(), List.class));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.nodeLabels = CypherMapWrapper.failOnNull("nodeLabels", config.getChecked("nodeLabels", MyPregelConfig.super.nodeLabels(), List.class));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.usernameOverride = StringUtils.trimToNull(config.getString("username", MyPregelConfig.super.usernameOverride()));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.sudo = config.getBool("sudo", MyPregelConfig.super.sudo());
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.concurrency = config.getInt("concurrency", MyPregelConfig.super.concurrency());
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.relationshipWeightProperty = RelationshipWeightConfig.validatePropertyName(config.getString("relationshipWeightProperty", MyPregelConfig.super.relationshipWeightProperty()));
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.maxIterations = config.requireInt("maxIterations");
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            this.writeConcurrency = config.getInt("writeConcurrency", MyPregelConfig.super.writeConcurrency());
        } catch (IllegalArgumentException e) {
            errors.add(e);
        }
        try {
            validateConcurrency();
        } catch (IllegalArgumentException e) {
            errors.add(e);
        } catch (NullPointerException e) {
        }
        try {
            validateWriteConcurrency();
        } catch (IllegalArgumentException e) {
            errors.add(e);
        } catch (NullPointerException e) {
        }
        if(!errors.isEmpty()) {
            if(errors.size() == 1) {
                throw errors.get(0);
            } else {
                String combinedErrorMsg = errors.stream().map(IllegalArgumentException::getMessage).collect(Collectors.joining(System.lineSeparator() + "\t\t\t\t", "Multiple errors in configuration arguments:" + System.lineSeparator() + "\t\t\t\t", ""));
                IllegalArgumentException combinedError = new IllegalArgumentException(combinedErrorMsg);
                errors.forEach(error -> combinedError.addSuppressed(error));
                throw combinedError;
            }
        }
    }

    @Override
    public long startNode() {
        return this.startNode;
    }

    @Override
    public long endNode() {
        return this.endNode;
    }

    @Override
    public String writeProperty() {
        return this.writeProperty;
    }

    @Override
    public String mutateProperty() {
        return this.mutateProperty;
    }

    @Override
    public boolean isAsynchronous() {
        return this.isAsynchronous;
    }

    @Override
    public Partitioning partitioning() {
        return this.partitioning;
    }

    @Override
    public List<String> relationshipTypes() {
        return this.relationshipTypes;
    }

    @Override
    public List<String> nodeLabels() {
        return this.nodeLabels;
    }

    @Override
    public void graphStoreValidation(GraphStore graphStore, Collection<NodeLabel> selectedLabels,
            Collection<RelationshipType> selectedRelationshipTypes) {
        relationshipWeightValidation(graphStore, selectedLabels, selectedRelationshipTypes);
    }

    @Override
    public @Nullable String usernameOverride() {
        return this.usernameOverride;
    }

    @Override
    public boolean sudo() {
        return this.sudo;
    }

    @Override
    public Collection<String> configKeys() {
        return Arrays.asList("startNode", "endNode", "writeProperty", "mutateProperty", "isAsynchronous", "partitioning", "relationshipTypes", "nodeLabels", "username", "sudo", "concurrency", "relationshipWeightProperty", "maxIterations", "writeConcurrency");
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("startNode", startNode());
        map.put("endNode", endNode());
        map.put("writeProperty", writeProperty());
        map.put("mutateProperty", mutateProperty());
        map.put("isAsynchronous", isAsynchronous());
        map.put("partitioning", org.neo4j.gds.beta.pregel.Partitioning.toString(partitioning()));
        map.put("relationshipTypes", relationshipTypes());
        map.put("nodeLabels", nodeLabels());
        map.put("username", usernameOverride());
        map.put("sudo", sudo());
        map.put("concurrency", concurrency());
        map.put("relationshipWeightProperty", relationshipWeightProperty());
        map.put("maxIterations", maxIterations());
        map.put("writeConcurrency", writeConcurrency());
        return map;
    }

    @Override
    public int concurrency() {
        return this.concurrency;
    }

    @Override
    public @Nullable String relationshipWeightProperty() {
        return this.relationshipWeightProperty;
    }

    @Override
    public int maxIterations() {
        return this.maxIterations;
    }

    @Override
    public int writeConcurrency() {
        return this.writeConcurrency;
    }

    public static MyPregelConfigImpl.Builder builder() {
        return new MyPregelConfigImpl.Builder();
    }

    public static final class Builder {
        private final Map<String, Object> config;

        public Builder() {
            this.config = new HashMap<>();
        }

        public MyPregelConfigImpl.Builder startNode(long startNode) {
            this.config.put("startNode", startNode);
            return this;
        }

        public MyPregelConfigImpl.Builder endNode(long endNode) {
            this.config.put("endNode", endNode);
            return this;
        }

        public MyPregelConfigImpl.Builder writeProperty(String writeProperty) {
            this.config.put("writeProperty", writeProperty);
            return this;
        }

        public MyPregelConfigImpl.Builder mutateProperty(String mutateProperty) {
            this.config.put("mutateProperty", mutateProperty);
            return this;
        }

        public MyPregelConfigImpl.Builder isAsynchronous(boolean isAsynchronous) {
            this.config.put("isAsynchronous", isAsynchronous);
            return this;
        }

        public MyPregelConfigImpl.Builder partitioning(Object partitioning) {
            this.config.put("partitioning", partitioning);
            return this;
        }

        public MyPregelConfigImpl.Builder relationshipTypes(List<String> relationshipTypes) {
            this.config.put("relationshipTypes", relationshipTypes);
            return this;
        }

        public MyPregelConfigImpl.Builder nodeLabels(List<String> nodeLabels) {
            this.config.put("nodeLabels", nodeLabels);
            return this;
        }

        public MyPregelConfigImpl.Builder usernameOverride(String usernameOverride) {
            this.config.put("username", usernameOverride);
            return this;
        }

        public MyPregelConfigImpl.Builder sudo(boolean sudo) {
            this.config.put("sudo", sudo);
            return this;
        }

        public MyPregelConfigImpl.Builder concurrency(int concurrency) {
            this.config.put("concurrency", concurrency);
            return this;
        }

        public MyPregelConfigImpl.Builder relationshipWeightProperty(
                String relationshipWeightProperty) {
            this.config.put("relationshipWeightProperty", relationshipWeightProperty);
            return this;
        }

        public MyPregelConfigImpl.Builder maxIterations(int maxIterations) {
            this.config.put("maxIterations", maxIterations);
            return this;
        }

        public MyPregelConfigImpl.Builder writeConcurrency(int writeConcurrency) {
            this.config.put("writeConcurrency", writeConcurrency);
            return this;
        }

        public MyPregelConfig build() {
            CypherMapWrapper config = CypherMapWrapper.create(this.config);
            return new MyPregelConfigImpl(config);
        }
    }
}
